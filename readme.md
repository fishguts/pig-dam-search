# Pig DAM Search

## Overview

`search` is a project that serves two purposes:

1. It's a project with a [CLI](./src/pigcmd.js)
2. It's a docker container that contains [elasticsearch](https://www.elastic.co/products/elasticsearch) configured with cores per each project.

One can think of the DAM as a versioned file system with search. The search facilitates search by meta properties which we discover when files are added and updated.  The indexer we are using is Elasticsearch.  The plan is to maintain on index per project/environment combination. This allows us to create types (analogous to a table) within. At the moment all I have planned is a `meta` type but I can see other possibilities such as `content`, `usage`.

We will designate one project/env per shard.  But the configuration details will come later.

## ID
Each project is identified but a URN: `urn:prj:[name]`.  Because we want to include the environment in the index we will tack on the environment: `urn:prj:[name]:[env]`.

## Types
`metadata`: search by metadata.
