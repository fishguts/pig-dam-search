/**
 * User: curtis
 * Date: 12/1/17
 * Time: 5:51 PM
 * Copyright @2017 by Xraymen Inc.
 */

const constant=require("../../src/common/constant");
const model_factory=require("../../src/data/factory");


exports.application={
	/**
	 * @returns {Application}
	 */
	get: ()=>model_factory.application.get(constant.nodenv.TEST)
};
