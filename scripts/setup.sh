#!/usr/bin/env bash
####################################################################################################
# capable of the following setup tasks:
# 	- mappings: sets up all elasticsearch mappings
####################################################################################################
# set -x

# setup defaults
ACTION=""
HOST="localhost:9200"
POSITIONAL=()

###########################################################################
# functional support
###########################################################################
function _usage {
	echo "Usage: ./scripts/setup.sh [--host host:port] <mappings>"
	echo "   mappings: sets up all elasticsearch mappings"
}

function _setupMapping {
	TEMPLATE_NAME="${1}"
	TEMPLATE_PATH="${2}"
	printf "setting up mapping template search/${TEMPLATE_PATH} on ${HOST}"
	# it takes a while to start this fella up so we just keep retrying until we succeed or the world gives up on us.
	while true
	do
		printf .
		curl -s -X PUT ${HOST}/_template/${TEMPLATE_NAME} -H "Content-Type: application/json" -d "@${TEMPLATE_PATH}" > /dev/null
		if [[ ${?} -eq 0 ]]
		then 
			exit 0
		fi
		sleep 1
	done
}

###########################################################################
# action
###########################################################################
# make sure we are where we think we are
if [[ -e "./setup.sh" ]]
then
	cd ..
elif [[ ! -e "./scripts/setup.sh" ]]
then
	echo "Error: must be run from project root"
	_usage
	exit 1
fi

# parse the command line
while [[ ${#} -gt 0 ]]
do
	case "$1" in
		"--host")
			shift;
			HOST="$1"
			shift;
			;;
		"mappings")
			ACTION="$1"
			shift;
			;;
		*)
			POSITIONAL+=("$1")
			shift;
			;;
	esac
done

# make it so
if [[ "${ACTION}" == "mappings" ]]
then
	_setupMapping "pig_metadata_template" "res/elasticmap/elasticmap-metadata.json"
else
	echo "Error: unknown action \"${ACTION}\""
	_usage
	exit 1
fi

