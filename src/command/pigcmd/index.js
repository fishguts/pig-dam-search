/**
 * User: curtis
 * Date: 11/19/17
 * Time: 2:19 PM
 * Copyright @2017 by Xraymen Inc.
 *
 * This is the command interface to <link>./src/pigcmd.js</link>
 */

const {Command}=require("pig-cmd").command;
const elastic_map=require("../../elastic/map");

/**
 * All actions supported by an implementing application
 * @type {Object<string, PigCliAction>}
 */
exports.ACTIONS={
	"buildmetamap": {
		args: "[library-path] [output-path]",
		desc: "adds a resource's type/metadata to our library",
		/**
		 * @param {Command} command
		 * @returns {function(callback)}
		 */
		handler: (command) => command._buildMetadataMap.bind(command),
		/**
		 * @param {Array<string>} position
		 * @param {Object<string, string>} options
		 * @throws {Error} - if you want to fail validation
		 */
		validate: function(position, options) {
			if(position.length>2) {
				throw new Error("takes two arguments at most");
			}
		}
	}
};

/**
 * These are a complete list of all of the options we support on a command line:
 * @type {Object<string, PigCliAction>}
 */
exports.OPTIONS=[

];

/**
 * Interface into application specific commands
 */
class CommandInterface extends Command {
	constructor(action, options, position) {
		super(options);
		this.action=action;
		this.position=position;
		this.execute=exports.ACTIONS[this.action].handler(this);
	}

	/**** action handlers ****/

	/**
	 * Adds metadata to our library
	 * @param {Function} callback
	 * @private
	 */
	_buildMetadataMap(callback) {
		// MetadataMap has intelligent defaults that we like (provided they are not overriden by command line)
		const options={};
		if(this.position.length>0) {
			options.sourcePath=this.position[0];
			if(this.position.length>1) {
				options.targetPath=this.position[1];
			}
		}
		const metaMap=new elastic_map.MetadataMap(options);
		metaMap.buildFromLibrary();
		process.nextTick(callback);
	}
}

exports.CommandInterface=CommandInterface;
