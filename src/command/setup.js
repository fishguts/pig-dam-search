/**
 * User: curtis
 * Date: 11/11/17
 * Time: 11:29 PM
 * Copyright @2017 by Xraymen Inc.
 *
 * Configures this applications settings. Where do the settings come from?
 *    - if the application is configured to <code>useLocalConfiguration</code> then it uses the configuration
 *      files found in <link>./res/configuration/</link>
 *    - if the application is NOT configured to <code>useLocalConfiguration</code> then it reaches out to the
 *      settings server to get his specs.
 */

const _=require("lodash");
const pig_cmd=require("pig-cmd");
const constant=require("../common/constant");
const http=require("../common/http");
const {setupEnvironment}=require("../common/setup");
const model_factory=require("../data/factory");

/**
 * Configures our application.
 */
class SetupEnvironmentCommand extends pig_cmd.command.Command {
	/**
	 * @param {Object} options
	 * @param {"cli"|"gui"|"server"} options.mode - see <code>constant.application.mode</code>
	 */
	constructor(options=undefined) {
		super(Object.assign({
			application: constant.application.mode.SERVER
		}, options));
	}

	commands() {
		const application=model_factory.application.get(_.pick(this.options, ["mode"]));
		if(application.useLocalConfiguration()) {
			setupEnvironment(application);
			return null;
		} else {
			const url=http.route.settings.clusterGet(),
				// note: we only fail startup if we cannot contact the settings server in the upper environments. We
				// don't want to force debugging to be dependent on the whole suite.
				errorPolicy=application.isLowerEnv
					? constant.severity.DEBUG
					: constant.severity.ERROR;
			return [
				new pig_cmd.http.request.HttpGetJsonRequestCommand(url, {errorPolicy: errorPolicy}),
				this
			];
		}
	}

	execute(callback, history) {
		// the last command is our request.HttpGetJsonRequestCommand up in this.commands()`
		const settings=history.last().result,
			application=(settings)
				? model_factory.application.set(settings, _.pick(this.options, ["mode"]))
				: model_factory.application.get(_.pick(this.options, ["mode"]));
		setupEnvironment(application);
		process.nextTick(callback);
	}
}

module.exports={
	SetupEnvironmentCommand
};
