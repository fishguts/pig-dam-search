/**
 * User: curtis
 * Date: 11/18/17
 * Time: 6:04 PM
 * Copyright @2017 by Xraymen Inc.
 */

const _=require("lodash");
const notification=require("pig-core").notification;
const urn=require("pig-core").urn;
const {SeriesQueue}=require("pig-cmd").queue;
const constant=require("./constant");

/**
 * Job queue
 */
class JobQueue {
	/**
	 * @param {Object} options
	 *  - name {String=undefined}
	 */
	constructor(options=undefined) {
		this._options=Object.assign({
			errorPolicy: constant.severity.WARN,
			traceId: urn.create(constant.urn.type.QUEUE, _.get(options, "name", "app-job-queue"))
		}, options);
		const self=this;
		this._timerId=null;
		this._queues={};
		_.values(constant.priority).forEach((key)=>this._queues[key]=[]);
		notification.addListener(constant.event.SHUTDOWN_START, function() {
			// give everybody a chance to react to this event and pack in any last deeds they
			// want done and then flush the queue.
			process.nextTick(self._process.bind(self));
		});
	}

	/**
	 * Adds command to the queue and executes it now, sooner or soon depending on priority.
	 * Note: when adding new commands the wait clock is reset.
	 * @param {Command} command
	 * @param {String} priority the lower the priority the more time between now and when the queue
	 *    wakes up to process this and any other commands that have been queued up.
	 */
	addCommand(command, priority=constant.priority.LOW) {
		this._queues[priority].push(command);
		if(priority===constant.priority.HIGH) {
			this._deferProcessing(0);
		} else if(priority===constant.priority.MEDIUM) {
			this._deferProcessing(100);
		} else {
			this._deferProcessing(200);
		}
	}

	/**** Private Interface ****/
	/**
	 * Defers processing of the entire queue by the amount specified
	 * @param {Number} millis
	 * @private
	 */
	_deferProcessing(millis) {
		const self=this;
		if(this._timerId!==null) {
			clearTimeout(this._timerId);
		}
		this._timerId=setTimeout(function() {
			self._timerId=null;
			self._process();
		}, millis);
	}

	/**
	 * Sets up our commands and in priority order and passes them on to a serial execution queue
	 * @private
	 */
	_process() {
		const series=new SeriesQueue(_.omit(this._options, ["name"]));
		series.addCommands(this._queues[constant.priority.HIGH]
			.concat(this._queues[constant.priority.MEDIUM])
			.concat(this._queues[constant.priority.LOW]));
		_.values(constant.priority).forEach((key)=>this._queues[key]=[]);
		series.execute();
	}
}

exports.Queue=JobQueue;
