/**
 * User: curtis
 * Date: 12/15/17
 * Time: 12:15 AM
 * Copyright @2017 by Xraymen Inc.
 */
const _=require("lodash");
const path=require("path");
const {PigError}=require("pig-core").error;
const log=require("pig-core").log;
const io=require("../common/io");
const model_factory=require("../data/factory");

class ElasticMap {
	/**
	 * @param {Object} options
	 * - input {Object|Input}
	 * - output {Output}
	 */
	constructor(options={}) {
		this.options=options;
	}
}

class MetadataMap extends ElasticMap {
	/**
	 * @param {Object} options
	 * - library {Input}
	 * - target {Output}
	 * - template (Input}
	 */
	constructor(options={}) {
		// create defaults and let options override them
		const defaults={};
		if(!options.library) {
			options.library=new io.Input({
				json: true,
				filePath: path.join(model_factory.store.get().share, "metadata-library.json")
			});
		}
		if(!options.template) {
			options.template=new io.Input({
				json: true,
				filePath: "./res/template/template-elasticmap-metadata.json"
			});
		}
		if(!options.target) {
			options.target=new io.Output({
				json: true,
				pretty: true,
				filePath: "./res/elasticmap/elasticmap-metadata.json"
			});
		}
		super(Object.assign(defaults, options));
	}

	/**
	 * Builds a map from the library which defaults to `${store.share}/metadata-library.json`
	 * @returns {Object}
	 * @throws {Error} if template or library cannot be found.
	 */
	buildFromLibrary() {
		const self=this,
			template=this.options.template.read(),
			library=this.options.library.read();
		if(_.isEmpty(library.directory)) {
			throw new PigError({
				message: "library file is empty"
			});
		} else {
			// we are going to merge our library of metadata into our template. The properties are all going to fall under
			// "template.mappings.metadata.properties" which is why we are pulling that guy out of our template.  Additionally,
			// we define some rules that we want to keep which is another reason we dig in there and pull him out.
			const templatePropertiesParent=template.mappings.metadata.properties.exifMetadata.properties;
			_.forIn(library.common, function(value, property) {
				const result=self._metaFieldToElasticField(value, property);
				if(result) {
					// merge it in with our existing property. Note: the existing property has override power. That's
					// why he's there. We only set properties that he has not dictated.
					if(templatePropertiesParent.hasOwnProperty(property)) {
						Object.assign(result, templatePropertiesParent[property]);
					} else {
						templatePropertiesParent[property]=result;
					}
				}
			});
		}
		self.options.target.write(template);
		return template;
	}

	/**** Private Interface *****/
	/**
	 * Builds field description for elasticsearch.  Defaults include_in_all to false.
	 * @param {Object} value
	 * @param {String} property
	 * @returns {{type: String}}
	 * @private
	 */
	_metaFieldToElasticField(value, property) {
		const result={};
		if(value.type.length>1) {
			// take the first one for now. See if it is a problem
			log.warn(`more than one type for ${property}`);
			result.type=value.type[0];
		} else {
			result.type=value.type[0];
		}
		if(result.type==="string") {
			result.type="text";
		} else if(result.type==="date") {
			result.format="yyyy:MM:dd HH:mm:ss||yyyy:MM:dd HH:mm:ssZ";
		}
		return result;
	}
}

exports.MetadataMap=MetadataMap;
